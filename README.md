# Equal Experts Corrency Converter Test #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* A currency converter class created in a TDD fashion
* Version - 5b8d0fd276b6d288905ed2f63a934e057e8feca2

### How do I get set up? ###

* Summary of set up
* `npm install`
* How to run tests
* `npm test`

### Further enhancements list ###

* Adding further safety checks on instantiation to protect against all unexpected data types
* Adding similar safety checks to addCurrencies

### Who do I talk to? ###

* [Prem Ghinde](mailto:prem@nearlydone.co.uk)