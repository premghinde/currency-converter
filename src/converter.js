class CurrencyConverter {
    constructor(pairs) {
        if (typeof pairs === 'string') {
            throw new Error('Instantiated with string')
        }
        if (Array.isArray(pairs)) {
            throw new Error('Instantiated with array')
        }
        this.pairs = pairs
    }

    toUSD(currency, amount) {
        if (!this.pairs.get(currency)) {
            throw new Error(`Unknown currency ${currency}`)
        }
        return amount / this.pairs.get(currency)
    }

    toCurrency(currency, dollars) {
        if (!this.pairs.get(currency)) {
            throw new Error(`Unknown currency ${currency}`)
        }
        return Math.floor(dollars * this.pairs.get(currency) * 100) / 100
    }

    addCurrencies(input, destinationCurrency) {
        const sum = input.reduce((one, two) => 
            this.toUSD(one.code, one.amount) +
            this.toUSD(two.code, two.amount))
        return this.toCurrency(destinationCurrency, sum)
    }
}

module.exports = { CurrencyConverter }
