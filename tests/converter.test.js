const { CurrencyConverter } = require('../src/converter');

const rates = new Map([
    ['AUD', 1.43534],
    ['CAD', 1.31715],
    ['CNY', 6.88191],
    ['COP', 3203.18],
    ['EUR', 0.87815],
    ['GBP', 0.78569],
    ['INR', 69.3492],
    ['MXN', 19.2316],
    ['MYR', 4.13785],
])

describe('Equal Experts Currency Converter', () => {
    describe('Bad input data handling', () => {
        test.each([
            ['String', 'string', 'Instantiated with string'],
            ['Array', [1 ,2], 'Instantiated with array'],
        ])('Fails on %s', (type, val, errorVal) => {
            expect(() => new CurrencyConverter(val)).toThrow(Error(errorVal))
        })
    })

    describe('Converting into USD',() => {
        let instance

        beforeEach(() => {
            instance = new CurrencyConverter(rates)
        })

        test('Unrecognised currency not accepted by function', () => {
            expect(() => instance.toUSD('JPY', 1000)).toThrow(Error('Unknown currency JPY'))
        })

        test.each([
            ['AUD', 0.696699039948723],
            ['CAD', 0.7592149717192422],
            ['CNY', 0.14530849720499106],
            ['COP', 0.00031218976142458434],
            ['EUR', 1.1387576154415533],
            ['GBP', 1.2727666127862134],
            ['INR', 0.014419777012568278],
            ['MXN', 0.05199775369704029],
            ['MYR', 0.2416713993982382],
        ])('1 %s converted to USD', (currency, result) => {
            expect(instance.toUSD(currency, 1)).toEqual(result)
        })

        test.each([
            ['AUD', 68.97320495492357],
            ['CAD', 75.16228220020498],
            ['CNY', 14.385541223294114],
            ['COP', 0.03090678638103385],
            ['EUR', 112.73700392871378],
            ['GBP', 126.00389466583512],
            ['INR', 1.4275579242442595],
            ['MXN', 5.147777616006988],
            ['MYR', 23.92546854042558],
        ])('99 %s converted to USD', (currency, result) => {
            expect(instance.toUSD(currency, 99)).toEqual(result)
        })
    })

    describe('Converting into currency', () => {
        let instance
        beforeEach(() => {
            instance = new CurrencyConverter(rates)
        })

        test('Unrecognised currency not accepted by function', () => {
            expect(() => instance.toCurrency('JPY', 1000)).toThrow(Error('Unknown currency JPY'))
        })

        test.each([
            ['AUD', 142.09],
            ['CAD', 130.39],
            ['CNY', 681.30],
            ['COP', 317114.82],
            ['EUR', 86.93],
            ['GBP', 77.78],
            ['INR', 6865.57],
            ['MXN', 1903.92],
            ['MYR', 409.64],
        ])('99 USD converted to %s', (currency, result) => {
            expect(instance.toCurrency(currency, 99)).toEqual(result)
        })
    })

    describe('Adding currencies', () => {
        let instance
        beforeEach(() => {
            instance = new CurrencyConverter(rates)
        })

        test('13.12 EUR added to 99 GBP and result output as CAD', () => {
            const eur = {
                code: 'EUR',
                amount: 13.12
            }
            const gbp = {
                code: 'GBP',
                amount: 99
            }
            expect(instance.addCurrencies([eur, gbp], 'CAD')).toEqual(185.64)
        })
    })
})